# Basket Animation Example
This project is an experimental animation for use in ecommerce projects. Its designed to give the end-user a more visual experience of what happens when you click on the "Add to basket" button of a product.

#### [Demo](https://epenance.gitlab.io/basketanimation/)

## The idea
 As a frontend developer I always look for ways to improve the experience users of the websites I build.
 
 I work a lot with E-commerce and wanted to give the users a more visual, and unique, representation of what happens when they press the "Add to basket" button.
 
 The project itself is a small prototype and not meant as a final product, its meant to give inspiration to other developers out there as to what you can do with a little imagination

### Follow me
If your interested in seeing more ideas feel free to give me a follow on Twitter [@MartinHobert](https://twitter.com/martinhobert)
